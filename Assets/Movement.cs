﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public Vector3 TravelDirection, aux;
    public Vector3 oldDirection = Vector3.zero;
    public float travelSpeed, oldSpeed = 0;
    [Range (0,1)]
    public float lerpFactor = 0;
    public float angle = 0f;
    public static Vector3 eulerRot = Vector3.zero;

    private float maxSpeed = 5f;
    private float rotationSpeed = 2f;

    // Update is called once per frame
    void Update()
    {
        MyTransform myTransform = GetComponent<MyTransform>();
        Vector3 forwardDirection = VectorMaths.EulerAnglesToDirection(myTransform.Rotation);
        if (oldDirection != Vector3.zero)
            myTransform.Position += oldDirection * Time.deltaTime * oldSpeed;
        if (Input.GetKey(KeyCode.Space))
        {
            travelSpeed += Time.deltaTime;
            lerpFactor += Time.deltaTime;
            lerpFactor = Mathf.Clamp(lerpFactor, 0, 1);
            if (travelSpeed > maxSpeed)
            {
                travelSpeed = maxSpeed;
            }

            if (oldDirection == Vector3.zero)
                TravelDirection = forwardDirection;
            else TravelDirection = VectorMaths.Lerp(oldDirection, forwardDirection, lerpFactor);
            myTransform.Position += TravelDirection * Time.deltaTime * travelSpeed;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            // aux = oldDirection;
            //oldDirection = TravelDirection;
            if (oldSpeed == 0)
                oldSpeed = travelSpeed;
            else oldSpeed += travelSpeed; /** Vector3.Dot(TravelDirection.normalized, aux.normalized);*/
            //oldSpeed = Mathf.Clamp(oldSpeed, 0, 2*maxSpeed);
            oldDirection = VectorMaths.Lerp(oldDirection, TravelDirection, Mathf.Min(oldSpeed, travelSpeed) / Mathf.Max(oldSpeed, travelSpeed));
            //if (oldDirection == TravelDirection)
            //    oldSpeed -= travelSpeed;
            oldSpeed = Mathf.Abs(oldSpeed);
            travelSpeed = 0;
            lerpFactor = 0;
        }
       

        if (Input.GetKey(KeyCode.W))
        {
            //eulerRot.x += Time.deltaTime; 
           myTransform.Rotation.x += Time.deltaTime * rotationSpeed;
           //myTransform.angularVelocity.x += Time.deltaTime * rotationSpeed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            myTransform.Rotation.x -= Time.deltaTime * rotationSpeed;
            //myTransform.angularVelocity.x -= Time.deltaTime * rotationSpeed;
        }
        if (Input.GetKey(KeyCode.A))
        {
            myTransform.Rotation.y -= Time.deltaTime * rotationSpeed;
            //myTransform.angularVelocity.y -= Time.deltaTime * rotationSpeed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            myTransform.Rotation.y += Time.deltaTime * rotationSpeed;
            //myTransform.angularVelocity.y += Time.deltaTime * rotationSpeed;
        }

    }



}
