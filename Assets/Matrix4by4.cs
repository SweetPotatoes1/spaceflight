﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matrix4by4
{

    public Matrix4by4(Vector4 column1, Vector4 column2, Vector4 column3, Vector4 column4)
    {
        values = new float[4, 4];
        values[0, 0] = column1.x;
        values[1, 0] = column1.y;
        values[2, 0] = column1.z;
        values[3, 0] = column1.w;

        values[0, 1] = column2.x;
        values[1, 1] = column2.y;
        values[2, 1] = column2.z;
        values[3, 1] = column2.w;

        values[0, 2] = column3.x;
        values[1, 2] = column3.y;
        values[2, 2] = column3.z;
        values[3, 2] = column3.w;

        values[0, 3] = column4.x;
        values[1, 3] = column4.y;
        values[2, 3] = column4.z;
        values[3, 3] = column4.w;
    }
    public Matrix4by4(Vector3 column1, Vector3 column2, Vector3 column3, Vector3 column4)
    {
        values = new float[4, 4];
        values[0, 0] = column1.x;
        values[1, 0] = column1.y;
        values[2, 0] = column1.z;
        values[3, 0] = 0;

        values[0, 1] = column2.x;
        values[1, 1] = column2.y;
        values[2, 1] = column2.z;
        values[3, 1] = 0;

        values[0, 2] = column3.x;
        values[1, 2] = column3.y;
        values[2, 2] = column3.z;
        values[3, 2] = 0;

        values[0, 3] = column4.x;
        values[1, 3] = column4.y;
        values[2, 3] = column4.z;
        values[3, 3] = 1;
    }
    public Matrix4by4()
    {
        values = new float[4, 4];
        values[0, 0] = 1;
        values[1, 0] = 0;
        values[2, 0] = 0;
        values[3, 0] = 0;

        values[0, 1] = 0;
        values[1, 1] = 1;
        values[2, 1] = 0;
        values[3, 1] = 0;

        values[0, 2] = 0;
        values[1, 2] = 0;
        values[2, 2] = 1;
        values[3, 2] = 0;

        values[0, 3] = 0;
        values[1, 3] = 0;
        values[2, 3] = 0;
        values[3, 3] = 0;
    }
    public float[,] values;
  
    
    public static Vector4 operator *(Matrix4by4 lhs, Vector4 vector)
    {
        //vector.w = 1;
        Vector4 ret;
        ret.x = lhs.values[0, 0] * vector.x + lhs.values[0, 1] * vector.y + lhs.values[0, 2] * vector.z + lhs.values[0, 3] * vector.w;
        ret.y = lhs.values[1, 0] * vector.x + lhs.values[1, 1] * vector.y + lhs.values[1, 2] * vector.z + lhs.values[1, 3] * vector.w;
        ret.z = lhs.values[2, 0] * vector.x + lhs.values[2, 1] * vector.y + lhs.values[2, 2] * vector.z + lhs.values[2, 3] * vector.w;
        ret.w = lhs.values[3, 0] * vector.x + lhs.values[3, 1] * vector.y + lhs.values[3, 2] * vector.z + lhs.values[3, 3] * vector.w;
        return ret;
    }

    public static Matrix4by4 operator *(Matrix4by4 lhs, Matrix4by4 rhs)
    {
        Matrix4by4 ret = new Matrix4by4(Vector4.zero, Vector4.zero, Vector4.zero, Vector4.zero);
        
            for (int i = 0; i < 4; i++)
            {
                //float element = 0f;
                for (int j = 0; j < 4; j++)
                {
                ret.values[i, j] = lhs.values[i, 0] * rhs.values[0, j] + lhs.values[i,1] * rhs.values[1,j] + lhs.values[i,2] * rhs.values[2,j] + lhs.values[i,3] * rhs.values[3,j];
                }

            }
      
        //ret.values[0, 0] = lhs.values[0, 0] * rhs.values[0, 0] + lhs.values[0, 1] * rhs.values[1, 0] + lhs.values[0, 2] * rhs.values[2, 0] + lhs.values[0, 3] * rhs.values[3, 0];
        return ret;
    }
  
}

