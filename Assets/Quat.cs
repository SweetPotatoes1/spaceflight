﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quat
{
    public float x, y, z, w;

    public Quat(float angle, Vector3 axis)
    {
        float halfAngle = angle / 2;
        w = Mathf.Cos(halfAngle);
        x = axis.x * Mathf.Sin(halfAngle);
        y = axis.y * Mathf.Sin(halfAngle);
        z = axis.z * Mathf.Sin(halfAngle);
    }
    public Quat(Vector3 axis)
    {
        w = 0f;
        x = axis.x;
        y = axis.y;
        z = axis.z;
    }
    public Quat()
    {
        w = 1;
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector3 getAxis()
    {
        Vector3 V = new Vector3();
        V.x = x;
        V.y = y;
        V.z = z;
        return V;
    }
    public void setAxis(Vector3 axis)
    {
        x = axis.x;
        y = axis.y;
        z = axis.z;
    }
    public Quat Inverse()
    {
        Quat rv = new Quat();
        rv.w = w;
        rv.setAxis(-getAxis());
        return rv;
    }

    public Vector3 QuatToEulerAngle(Quat q)
    {
        Vector3 rv;

        float sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
        float cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
        rv.x = Mathf.Atan2(sinr_cosp, cosr_cosp);

        // pitch (y-axis rotation)
        float sinp = 2 * (q.w * q.y - q.z * q.x);
        if (Mathf.Abs(sinp) >= 1)
            rv.y = Mathf.PI / 2 * sinp; // use 90 degrees if out of range
        else
            rv.y = Mathf.Asin(sinp);

        // yaw (z-axis rotation)
        float siny_cosp = 2 * (q.w * q.z + q.x * q.y);
        float cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
        rv.z = Mathf.Atan2(siny_cosp, cosy_cosp);
        return rv;
    }
    public static Quat EulerAngleToQuat(Vector3 EulerAngle)
    {
        float cy = Mathf.Cos(EulerAngle.y * 0.5f);
        float sy = Mathf.Sin(EulerAngle.y * 0.5f);
        float cp = Mathf.Cos(EulerAngle.x * 0.5f);
        float sp = Mathf.Sin(EulerAngle.x * 0.5f);
        float cr = Mathf.Cos(EulerAngle.z * 0.5f);
        float sr = Mathf.Sin(EulerAngle.z * 0.5f);

        Quat q = new Quat();

        q.w = cy * cp * cr + sy * sp * sr;
        q.z = cy * cp * sr - sy * sp * cr;
        q.x = sy * cp * sr + cy * sp * cr;
        q.y = sy * cp * cr - cy * sp * sr;

        return q;
    }

    public static Matrix4by4 QuatToMatrix(Quat Q)
    {
        Matrix4by4 mat;
        float[] vals = new float[16];
        float xx, xy, xz, xw, yy, yz, yw, zz, zw;
        xx = Q.x * Q.x;
        xy = Q.x * Q.y;
        xz = Q.x * Q.z;
        xw = Q.x * Q.w;

        yy = Q.y * Q.y;
        yz = Q.y * Q.z;
        yw = Q.y * Q.w;

        zz = Q.z * Q.z;
        zw = Q.z * Q.w;
  
        vals[0] = 1 - 2 * (yy + zz);
        vals[1] = 2 * (xy - zw);
        vals[2] = 2 * (xz + yw);

        vals[4] = 2 * (xy + zw);
        vals[5] = 1 - 2 * (xx + zz);
        vals[6] = 2 * (yz - xw);
        
        vals[8] = 2 * (xz - yw);
        vals[9] = 2 * (yz + xw);
        vals[10] = 1 - 2 * (xx + yy);
       
        vals[3] = vals[7] = vals[11] = vals[12] = vals[13] = vals[14] = 0;
        vals[15] = 1;
        mat = new Matrix4by4(new Vector4(vals[0], vals[1], vals[2], vals[3]),
                             new Vector4(vals[4], vals[5], vals[6], vals[7]),
                             new Vector4(vals[8], vals[9], vals[10], vals[11]),
                             new Vector4(vals[12], vals[13], vals[14], vals[15]));
        return mat;
    }
    public static Quat operator*(Quat lhs, Quat rhs)
    {
        float angle = ((rhs.w * lhs.w) - Vector3.Dot(rhs.getAxis(), lhs.getAxis()));
        Vector3 axis = rhs.w * lhs.getAxis() + lhs.w * rhs.getAxis() + Vector3.Cross(lhs.getAxis(),rhs.getAxis());
        Quat k = new Quat();
        k.w = angle;
        k.setAxis(axis);
        return k;
    }

}
