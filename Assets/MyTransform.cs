﻿using UnityEngine;

public class MyTransform : MonoBehaviour
{
    ///*************FIELDS**********************
    public Vector3 Position, Rotation, Scale;
    public GameObject Parent;
    public float angle = 0f;
    public Vector3 angularVelocity;
    private Vector3[] ModelSpaceVertices;
    private Vector3 m_localAxis;
    public Quat orientation = new Quat();
    ///*************METHODS*********************
    
    void Start()
    {
        ///Scale = new Vector3(1, 1, 1);
        MeshFilter MF = GetComponent<MeshFilter>();
        ModelSpaceVertices = MF.mesh.vertices;
    }
    Matrix4by4 calculateMatrices()
    {
        Matrix4by4 rollMatrix = new Matrix4by4(new Vector3(Mathf.Cos(Rotation.z), Mathf.Sin(Rotation.z), 0),
                                           new Vector3(-Mathf.Sin(Rotation.z), Mathf.Cos(Rotation.z), 0),
                                           new Vector3(0, 0, 1),
                                           Vector3.zero);

        Matrix4by4 pitchMatrix = new Matrix4by4(new Vector3(1, 0, 0),
                                                new Vector3(0, Mathf.Cos(Rotation.x), Mathf.Sin(Rotation.x)),
                                                new Vector3(0, -Mathf.Sin(Rotation.x), Mathf.Cos(Rotation.x)),
                                                Vector3.zero);

        Matrix4by4 yawMatrix = new Matrix4by4(new Vector3(Mathf.Cos(Rotation.y), 0, -Mathf.Sin(Rotation.y)),
                                              new Vector3(0, 1, 0),
                                              new Vector3(Mathf.Sin(Rotation.y), 0, Mathf.Cos(Rotation.y)),
                                              Vector3.zero);

        Matrix4by4 scaleMatrix = new Matrix4by4(new Vector3(Scale.x, 0, 0), new Vector3(0, Scale.y, 0), new Vector3(0, 0, Scale.z), Vector3.zero);

        Matrix4by4 translationMatrix = new Matrix4by4(new Vector4(1, 0, 0, 0), new Vector4(0, 1, 0, 0), new Vector4(0, 0, 1, 0), new Vector4(Position.x, Position.y, Position.z, 1));

        Matrix4by4 R = yawMatrix * (pitchMatrix * rollMatrix);
        Matrix4by4 M = translationMatrix * (R * scaleMatrix);
        return M;
    }

    Matrix4by4 calculateLocalMatrices()
    {
        if (Parent != null)
        {
            MyTransform parentTransform = Parent.GetComponent<MyTransform>();

            Matrix4by4 rollMatrix = new Matrix4by4(new Vector3(Mathf.Cos(parentTransform.Rotation.z), Mathf.Sin(parentTransform.Rotation.z), 0),
                                               new Vector3(-Mathf.Sin(parentTransform.Rotation.z), Mathf.Cos(parentTransform.Rotation.z), 0),
                                               new Vector3(0, 0, 1),
                                               Vector3.zero);

            Matrix4by4 pitchMatrix = new Matrix4by4(new Vector3(1, 0, 0),
                                                    new Vector3(0, Mathf.Cos(parentTransform.Rotation.x), Mathf.Sin(parentTransform.Rotation.x)),
                                                    new Vector3(0, -Mathf.Sin(parentTransform.Rotation.x), Mathf.Cos(parentTransform.Rotation.x)),
                                                    Vector3.zero);

            Matrix4by4 yawMatrix = new Matrix4by4(new Vector3(Mathf.Cos(parentTransform.Rotation.y), 0, -Mathf.Sin(parentTransform.Rotation.y)),
                                                  new Vector3(0, 1, 0),
                                                  new Vector3(Mathf.Sin(parentTransform.Rotation.y), 0, Mathf.Cos(parentTransform.Rotation.y)),
                                                  Vector3.zero);

            Matrix4by4 scaleMatrix = new Matrix4by4(new Vector3(Scale.x, 0, 0), new Vector3(0, Scale.y, 0), new Vector3(0, 0, Scale.z), Vector3.zero);

            Matrix4by4 translationMatrix = new Matrix4by4(new Vector4(1, 0, 0, 0), new Vector4(0, 1, 0, 0), new Vector4(0, 0, 1, 0), new Vector4(parentTransform.Position.x - Position.x, parentTransform.Position.y - Position.y, parentTransform.Position.z - Position.z, 1));

            Matrix4by4 R = yawMatrix * (pitchMatrix * rollMatrix);
            Matrix4by4 M = translationMatrix * (R * scaleMatrix);
            return M;
        }
        else return new Matrix4by4();
        
    }

    void updateVertices(Matrix4by4 M)
    {
        Vector3[] TransformedVertices = new Vector3[ModelSpaceVertices.Length];

        for (int i = 0; i < TransformedVertices.Length; i++)
        {
            Vector4 newModelSpaceVertex = new Vector4(ModelSpaceVertices[i].x, ModelSpaceVertices[i].y, ModelSpaceVertices[i].z, 1);
            TransformedVertices[i] = M * newModelSpaceVertex;

        }

        MeshFilter MF = GetComponent<MeshFilter>();
        MF.mesh.vertices = TransformedVertices;
        MF.mesh.RecalculateBounds();
        MF.mesh.RecalculateNormals();
    }

    void updateVerticesQ()
    {   
        Vector3[] TransformedVertices = new Vector3[ModelSpaceVertices.Length];

        Quat RotationQuat = Quat.EulerAngleToQuat(Rotation);

        //RotationQuat *= Quat.EulerAngleToQuat(Rotation);

        //Quat q = new Quat(angle, new Vector3(0, 1, 0));


        for (int i = 0; i < TransformedVertices.Length; i++)
        {
            Quat K = new Quat(ModelSpaceVertices[i]);
            Quat newK = RotationQuat * K * RotationQuat.Inverse();
            Vector3 newP = newK.getAxis();
            TransformedVertices[i] = newP;
        }
        
        MeshFilter MF = GetComponent<MeshFilter>();
        MF.mesh.vertices = TransformedVertices;
        MF.mesh.RecalculateBounds();
        MF.mesh.RecalculateNormals();

    }


    private void Update()
    {
       //angle += Time.deltaTime;
       updateVerticesQ();
       //updateVertices(calculateMatrices());
       //if(Parent!=null)
       //updateVertices(calculateLocalMatrices());
    }
    //private void FixedUpdate()
    //{
    //    float avMag = (angularVelocity * Time.fixedDeltaTime).magnitude;
    //    Quat q = new Quat();
    //    if (avMag != 0)
    //    {
    //        q.w = Mathf.Cos(avMag / 2);
    //        q.x = Mathf.Sin(avMag / 2) * (angularVelocity * Time.fixedDeltaTime).x / avMag;
    //        q.y = Mathf.Sin(avMag / 2) * (angularVelocity * Time.fixedDeltaTime).y / avMag;
    //        q.z = Mathf.Sin(avMag / 2) * (angularVelocity * Time.fixedDeltaTime).z / avMag;
    //    }

    //    Rotation = q.QuatToEulerAngle(q);
    //}


}
