﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorMaths
{

    public static float magnitude(Vector3 V)
    {
        float ret = Mathf.Sqrt(V.x * V.x + V.y * V.y + V.z * V.z);
        return ret;
    }

    public static float magnitudeSQ(Vector3 V)
    {
        float ret = V.x * V.x + V.y * V.y + V.z * V.z;
        return ret;
    }
    public static Vector3 Lerp(Vector3 V1, Vector3 V2, float lerpFactor)
    {
        Vector3 rv;
        lerpFactor = Mathf.Clamp01(lerpFactor);
        rv = V1 * (1 - lerpFactor) + V2 * lerpFactor;
        return rv;
    }

    public static float DotProduct(Vector3 V1, Vector3 V2)
    {
        float ret;
        V1 = Normalize(V1);
        V2 = Normalize(V2);

        ret = V1.x * V2.x + V1.y * V1.y + V1.z * V2.z;
        return ret;
    }

    public static Vector3 CrossProduct(Vector3 V1, Vector3 V2)
    {
        Vector3 rv;
        rv.x = V1.y * V2.z + V1.z * V2.y;
        rv.y = V1.x * V2.z + V1.z * V2.x;
        rv.z = V1.x * V2.y + V1.y * V2.x;
        return rv;
    }

    public static float Distance(Vector3 V1, Vector3 V2)
    {
        float ret = Mathf.Sqrt((V1.x - V2.x) * (V1.x - V2.x)
                             + (V1.x - V2.x) * (V1.x - V2.x)
                             + (V1.x - V2.x) * (V1.x - V2.x));
        return ret;
    }

    public static float squaredDistance(Vector3 V1, Vector3 V2)
    {
        float ret = (V1.x - V2.x) * (V1.x - V2.x) +
                    (V1.x - V2.x) * (V1.x - V2.x) +
                    (V1.x - V2.x) * (V1.x - V2.x);
        return ret;
    }

    public static Vector3 Normalize(Vector3 V)
    {
        Vector3 rv;
        rv.x = V.x / magnitude(V);
        rv.y = V.y / magnitude(V);
        rv.z = V.z / magnitude(V);

        return rv;
    }
    public static Vector3 EulerAnglesToDirection(Vector3 EulerAngles)
    {
        Vector3 v = new Vector3();
        v.x = Mathf.Cos(EulerAngles.x) * Mathf.Sin(EulerAngles.y);
        v.y = -Mathf.Sin(EulerAngles.x);
        v.z = Mathf.Cos(EulerAngles.y) * Mathf.Cos(EulerAngles.x);
        return v;
    }
    public static float VectorToRadians(Vector2 V)
    {
        float rv = 0f;

        rv = Mathf.Atan(V.y / V.x);
        return rv;
    }
    public static Vector2 RadiansToVector(float angle)
    {
        Vector2 V = new Vector2();
        V.x = Mathf.Cos(angle);
        V.y = Mathf.Sin(angle);
        return V;
    }
}
